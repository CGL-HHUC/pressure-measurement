# pressure-measurement

## 介绍

使用力感科技的薄膜传感器 RP-C 18.3 LT 和力感科技的蓝牙传输模块作为硬件模块，该硬件模块可以通过手机 app 读到数值，也可通过串口调试的方法获得数值。
但硬件模块所读取的数值为 AD 值，并非真实的压力数值、压强数值。

之前使用流程：

1. 通过串口分析工具获得 AD 值；
2. 使用代码转换为对应的压力数值；

新的流程：

1. 运行程序时指定 COM 路径、波特率、数据保存路径；
2. 一次性保存多个文件，包括：串口获得的元数据、表格表示的传感器和对应的压力数据、传感器对应的压强数据；
3. 保存文件需指定路径 path 和文件名前缀 base，路径不存在会创建，保存文件为： path/base_raw.txt, path/base_p.csv, path/base_iop.csv;
4. 后续可直接将 csv 文件读入 python 或者 matlab 做分析。

### 开发环境

Win10 + WSL Ubuntu 16.04 LTS

### Usage

```shell
pressure <COM NUMBER> <OUTPUT FLODER> <OUTPUT BASENAME>
e.g. pressure /dev/ttyS13 ./test/ first
which will create and output data into floder
test/first_raw.txt
test/first_p.csv
test/first_iop.csv
```
