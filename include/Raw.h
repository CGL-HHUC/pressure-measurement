#pragma once

#include <unistd.h>
#include <stdio.h>
#include <errno.h>

#include <iostream>
#include <string>

#include "util.h"

using namespace std;

class Raw
{
public:
    Raw(string path):filepath(path),fd(-1){
        fd = openOutputFile(path);
    }
    ~Raw(){
        if(fd >= 0) {
            close(fd);
        }
    }

    void append(string line)
    {
        line.push_back('\n');
        write(fd, line.c_str(), line.size());
    }

    const string filepath;
private:
    int fd;    
};