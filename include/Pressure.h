#pragma once

#include <errno.h>
#include <unistd.h>

#include <string>
#include <vector>
#include <iostream>

#include "util.h"

using namespace std;

class PressureRecord
{
public:
    PressureRecord(string pressurePath, string iopPath)
        : pressurePath(pressurePath), iopPath(iopPath), pressureFd(-1), iopFd(-1)
    {
        this->square = 8.05 * 8.05 * 3.1415926 * 1e-6;
        pressureFd = openOutputFile(pressurePath);
        iopFd = openOutputFile(iopPath);
    }

    ~PressureRecord()
    {
        if (pressureFd >= 0)
        {
            close(pressureFd);
        }
        if (iopFd >= 0)
        {
            close(iopFd);
        }
    }

    void append(string line)
    {
        vector<int> ad = getADValue(line);

        string pressureStr;
        string iopStr;

        for (int item : ad)
        {
            float pressure = ad2Pressure(item);
            pressureStr.append(to_string(pressure));
            pressureStr.append(", ");

            float iop = ad2Iop(item);
            iopStr.append(to_string(iop));
            iopStr.append(", ");
        }

        if (pressureStr.size() > 2)
        {
            pressureStr = pressureStr.substr(0, pressureStr.size() - 2);
            pressureStr.push_back('\n');
        }

        if (iopStr.size() > 2)
        {
            iopStr = iopStr.substr(0, iopStr.size() - 2);
            iopStr.push_back('\n');
        }

        cout << "Pressure: " << pressureStr << "\tiop: " << iopStr << endl;

        if (pressureFd >= 0)
        {
            write(pressureFd, pressureStr.c_str(), pressureStr.size());
        }
        if (iopFd >= 0)
        {
            write(iopFd, iopStr.c_str(), iopStr.size());
        }
    }

    // 将字符串分割为 AD 值
    // 1,2,3,4,5,6; -> [1,2,3,4,5,6]
    vector<int> getADValue(string &line)
    {
        vector<int> result;
        string str = line.substr(0, line.size() - 1);
        vector<string> adStrList = splitString(str, SEPARATOR);

        for (string num : adStrList)
        {
            int value = std::stoi(num);
            result.push_back(value);
        }
        return result;
    }

    // 压力路径
    const string pressurePath;

    // 压强路径
    const string iopPath;
    static const char SEPARATOR;

    // 传感器面积 单位 m^2
    double square;

private:
    // 压力文件描述符
    int pressureFd;

    // 压强文件描述符
    int iopFd;

    // 计算压力值
    float ad2Pressure(int ad)
    {
        return 8.8 * ad / 100;
    }

    // 计算压强值 kpa
    float ad2Iop(int ad)
    {
        return ad2Pressure(ad) / square * 1e-3; // kpa
    }
};