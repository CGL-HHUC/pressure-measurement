#pragma once

#include <string>
#include <vector>
#include <functional>

using namespace std;

/**
 * 传入数据格式
 * 1. 两个数据之间以 ',' 隔开
 * 2. 每条数据以 ';' 隔开
 * 3. 数据个数 0~6 个
 * 4. 顺便记录下收到数据的时间
 * 
 * 0,50,0,170
 * ,0;
 * 1,1,0,0,0;
 * 0,1,1,0,0;
 * 0,599,3,57
 * 9,0;
 * 0,11,0,0,0;
 */

class BluLinkReceiver
{
public:
    static const char LINE_SEPARATOR;

    // 读到新的串口数据
    void append(string content);

    // 添加回调函数
    void addCallback(function<void(string)> decoder);

    // 移除回调函数
    void removeCallback(function<void(string)> decoder);

private:
    string buffer;
    vector<function<void(string)>> callbacks;
};