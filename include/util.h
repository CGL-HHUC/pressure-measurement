#pragma once

#include <string>
#include <vector>

using namespace std;

int openOutputFile(string file);
vector<string> splitString(const std::string& str, char tag);
void checkDirExist(string path);