#!/bin/bash

outputPath='./exp/'
PROGRAM=$PWD/build/bin/pressure
RecordTime=20
COM_NUM=/dev/ttyS13

if [ $# -ge 1 ]
then
outputPath=$1
fi

# $1 COM num
# $2 outputPath
# $3 basename
# $4 record seconds
function measure() {
    echo "${PROGRAM} $1 $2 $3 $4"
    ${PROGRAM} $1 $2 $3 $4
}

echo "the experiments data will save into ${outputPath}"

read -p "input ENTER to start measure left foot" ANSWER
echo "${ANSWER}"
measure ${COM_NUM} ${outputPath} 'left_foot' ${RecordTime}

read -p "input ENTER to start measure right foot" ANSWER
echo "${ANSWER}"
measure ${COM_NUM} ${outputPath} 'right_foot' ${RecordTime}

read -p "input ENTER to start measure left foot with insole" ANSWER
echo "${ANSWER}"
measure ${COM_NUM} ${outputPath} 'left_insole' ${RecordTime}

read -p "input ENTER to start measure right foot with insole" ANSWER
echo "${ANSWER}"
measure ${COM_NUM} ${outputPath} 'right_insole' ${RecordTime}
