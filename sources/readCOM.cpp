#include <cerrno>
#include <fcntl.h>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <termios.h>
#include <unistd.h>
#include <ctime>

#include <iostream>
#include <string>
#include <functional>
#include <filesystem>

#include "blulink.h"
#include "Raw.h"
#include "Pressure.h"

using namespace std;

int set_interface_attribs(int fd, int speed)
{
    struct termios tty;

    if (tcgetattr(fd, &tty) < 0)
    {
        printf("Error from tcgetattr: %s\n", strerror(errno));
        return -1;
    }

    cfsetospeed(&tty, (speed_t)speed);
    cfsetispeed(&tty, (speed_t)speed);

    tty.c_cflag |= (CLOCAL | CREAD); /* ignore modem controls */
    tty.c_cflag &= ~CSIZE;
    tty.c_cflag |= CS8;      /* 8-bit characters */
    tty.c_cflag &= ~PARENB;  /* no parity bit */
    tty.c_cflag &= ~CSTOPB;  /* only need 1 stop bit */
    tty.c_cflag &= ~CRTSCTS; /* no hardware flowcontrol */

    /* setup for non-canonical mode */
    tty.c_iflag &= ~(IGNBRK | BRKINT | PARMRK | ISTRIP | INLCR | IGNCR | ICRNL | IXON);
    tty.c_lflag &= ~(ECHO | ECHONL | ICANON | ISIG | IEXTEN);
    tty.c_oflag &= ~OPOST;

    /* fetch bytes as they become available */
    tty.c_cc[VMIN] = 1;
    tty.c_cc[VTIME] = 1;

    if (tcsetattr(fd, TCSANOW, &tty) != 0)
    {
        printf("Error from tcsetattr: %s\n", strerror(errno));
        return -1;
    }
    return 0;
}

void set_mincount(int fd, int mcount)
{
    struct termios tty;

    if (tcgetattr(fd, &tty) < 0)
    {
        printf("Error tcgetattr: %s\n", strerror(errno));
        return;
    }

    tty.c_cc[VMIN] = mcount ? 1 : 0;
    tty.c_cc[VTIME] = 5; /* half second timer */

    if (tcsetattr(fd, TCSANOW, &tty) < 0)
        printf("Error tcsetattr: %s\n", strerror(errno));
}

void test(string line)
{
    cout << line << endl;
}

int main(int argc, char* argv[])
{

    if(argc < 4)
    {
        printf("Execute command error!\nThere need more param\n");
        printf("Command demo: pressure <COM_PATH> <Output Path> <File Base Name> <record seconds>\n");
        exit(1);
    }

    // COM port name
    char *portName = argv[1];

    // output path
    string outputPath(argv[2]);
    checkDirExist(outputPath);
    
    // record basename
    string baseName(argv[3]);
    filesystem::path rawPath(outputPath);
    rawPath.append(baseName + "_raw.txt");
    filesystem::path pressurePath(outputPath);
    pressurePath.append(baseName + "_p.csv");
    filesystem::path iopPath(outputPath);
    iopPath.append(baseName + "_iop.csv");

    int fd;
    int wlen;
    char readBuffer[80];
    int readLength = -1;

    // 记录时长
    int recordTime = -1;
    if(argc >= 5) {
        recordTime = std::stoi(string(argv[4]));
    }

    fd = open(portName, O_RDWR | O_NOCTTY | O_SYNC);
    if (fd < 0)
    {
        printf("Error opening %s: %s\n", portName, strerror(errno));
        return -1;
    }
    /*baudrate 115200, 8 bits, no parity, 1 stop bit */
    set_interface_attribs(fd, B9600);
    //set_mincount(fd, 0);                /* set to pure timed read */

    // 读串口数据
    BluLinkReceiver receiver;
    receiver.addCallback(bind(&test, placeholders::_1));

    Raw raw(rawPath.string());    
    receiver.addCallback(bind(&Raw::append, &raw, placeholders::_1));

    PressureRecord pressureRecord(pressurePath.string(), iopPath.string());
    receiver.addCallback(bind(&PressureRecord::append, &pressureRecord, placeholders::_1));
    
    struct timespec startTime;
    struct timespec endTime;

    clock_gettime(CLOCK_REALTIME, &startTime);

    do
    {
        readLength = read(fd, readBuffer, sizeof(readBuffer) - 1);
        if(readLength == -1)
        {
            cout << "error: " << strerror(errno) << endl;
            continue;
        }

        readBuffer[readLength] = '\0'; 
        receiver.append(string(readBuffer));

        clock_gettime(CLOCK_REALTIME, &endTime);
        printf("cost time %ld\n", endTime.tv_sec-startTime.tv_sec);
        
        // 配置记录时长
        if(recordTime > 0 && endTime.tv_sec-startTime.tv_sec > recordTime) {
            printf("Record more than %d S\nStoping.\n", recordTime);
            break;
        }

    } while (1);
}