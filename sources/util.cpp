#include "util.h"

#include <unistd.h>
#include <fcntl.h>
#include <string.h>

#include <iostream>
#include <filesystem>

int openOutputFile(string file)
{
    int fd = open(file.c_str(), O_RDWR | O_CREAT);
    if (fd == -1)
    {
        cout << "open file error <" << file << "> " << strerror(errno) << endl;
        abort();
    }

    return fd;
}

vector<string> splitString(const std::string& str, char tag)
{
    vector<string> li;
    std::string subStr;
 
    //遍历字符串，同时将i位置的字符放入到子串中，当遇到tag（需要切割的字符时）完成一次切割
    //遍历结束之后即可得到切割后的字符串数组
    for(size_t i = 0; i < str.length(); i++)
    {
        if(tag == str[i]) //完成一次切割
        {
            if(!subStr.empty())
            {
                li.push_back(subStr);
                subStr.clear();
            }
        }
        else //将i位置的字符放入子串
        {
            subStr.push_back(str[i]);
        }
    }
 
    if(!subStr.empty()) //剩余的子串作为最后的子字符串
    {
        li.push_back(subStr);
    }
 
    return li;
}

// 确保路径存在
void checkDirExist(string path)
{
    if(filesystem::exists(path))
    {
        return;
    }

    filesystem::create_directories(path);
}