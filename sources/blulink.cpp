#include "blulink.h"

#include <algorithm>

const char BluLinkReceiver::LINE_SEPARATOR = ';';

void BluLinkReceiver::append(string content)
{
    // 检查是否能够组成完整的行数据
    size_t idx = content.find(LINE_SEPARATOR);

    if (idx == string::npos)
    {
        this->buffer.append(content);
        return;
    }
    else
    {
        // 获得完整一行
        string line = this->buffer;
        line.append(content.substr(0, idx + 1));
        this->buffer.clear();

        // 如果 ';' 后还有数据的话
        if (idx + 1 < content.size())
        {
            string restContent = content.substr(idx + 1, content.size() - idx - 1);
            this->buffer.append(restContent);
        }

        // 调用回调函数
        for (function<void(string)> func : this->callbacks)
        {
            func(line);
        }
    }
}

void BluLinkReceiver::addCallback(function<void(string)> decoder)
{
    this->callbacks.push_back(decoder);
}

void BluLinkReceiver::removeCallback(function<void(string)> decoder)
{
    for (vector<function<void(string)>>::iterator iter = this->callbacks.begin();
         iter != this->callbacks.end(); )
    {
        if (*decoder.target<void(*)(string)>() == *((*iter).target<void(*)(string)>()))
        {
            iter = this->callbacks.erase(iter);
        } else {
            iter++;
        }
    }
}